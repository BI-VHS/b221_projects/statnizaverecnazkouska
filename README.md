# StatniZaverecnaZkouska

## Popis

Dokážeš přežít **_státnice_**!?!?

Tahle hororová hra se ti rozhodně zaryje do paměti! Přiblíží ti hrůzu státnic, kterou každý rok pociťují desítky studentů **FIT ČVUT**. Ve hře se ocitneš v roli studenta, který se sice nestihl pořádně připravit, ale i přes hrůzostrašnou atmosféru to nevzdává.

Po příchodu do školy a seznámení se s pravidly státnic u Adély 💖, se dozvídáš, že studenti, kteří státnicovali před tebou, nechali na chodbách taháky. Nasbírej jich co nejvíc a nenech se chytit dozorem!

Na potítku se hrůzou zapotíš! Dokážeš něco opsat od spolužáků? Nebo je dokonce přesvědčit ať ti poradí? Dávej pozor! Dozor tě sleduje jako oko Sauronovo!

Prezentace bakalářky je převážně zkouška vyrovnanosti. Udrž klid, nepanikař a...  
<!-- TODO: vymyslet co za minihru tady bude a dopsat sem nejakej horor popis, co treba tetris? -->.

Prichází poslední část tvé zkoušky! Nasbíral si dost taháků a pomoci abys prošel? Zkoušející pokládají jednu zákeřnou otázku za druhou. Nevzdávej to!

## Hrát

- [Windows](./Builds/SZZ-windows.zip)

## Tým

| Jméno             | Role                                         |
| ----------------- | -------------------------------------------- |
| Jorge Zuniga      | Organizace / příběh / dialogy / programovaní |
| Kristýna Janovská | příběh / 3D / programovaní                   |
| Martin Steinbach  | sound design a hudba                         |
| Michal Černý      | 3D / programovaní                            |

## Dokumentace

1. [Statický svět](./documentation1.md)
2. [Dynamický svět](./documentation2.md)
3. [Finální dokumentace](./documentation3.md)

## Pitch

- [Game design document](https://docs.google.com/document/d/1AlGy7d5b9_4m-SRhOclgcIqjQWzRi1bnEQsVX--ewn0/edit?usp=sharing)
- [Flowchart](https://drive.google.com/file/d/1C4b__WXg4cSpRfkyvTw65HCRQ7Ws5G02/view?usp=sharing)
