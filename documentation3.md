# Dokumentace finální verze

## Průchod

Hráč se ocitá na 10. patře před studentkou, která má státnice - jako asi jediná z jejích přátel - za sebou. Zastaví se u Adély v kanceláři, která mu vysvětlí o co půjde, a pak je předhozen napospas dozoru v první fázi hry. V tuto chvíli je cílem nasbírat až 5 taháků, se kterými možná bude mít alespoń naději zkoušku dokončit. Jeho termín se ale rychle blíží a jestli ho dosud nedostali, musí spěchat na potítko o patro výše, kde se bude zase snažit vytěžit jakékoliv vědomosti ze spolužáků. Všechny tyto nápovědy se mu budou určitě hodit, když se vydá se na až na úpatí - do 14. patra ke komisi...

![12.png](./Screenshots/12.PNG)

## Interaktivita

Po patrech budovy TH:A jsou rozeseta těla nebohých studentů, která možná skrývají chtěné taháky. Zabavené taháky ale i občas vytrousí dozor z kapsy, není tedy nejlepší se před nimi jen schovávat.

![page-pickup](./Screenshots/page-pickup.gif)
![student-cheat-drop.png](./Screenshots/student-cheat-drop.png)

Většina studentů se i ráda dá ve volné chvíli s hráčem do řeči.


![student-dialogue.png](./Screenshots/student-dialogue.png)

## Implementace

Hra byla vytvořena v Unity s pomocí Blenderu a dalších programů. Vypíchnutí pár zasádaních částí:

### Generování modelů pater

V Blendru máme geometry nodes setup, který nám z jednoduchého obtažení plánku patra vytáhne zdi i nastaví materiály. Díry jako dvěre a okna jsou pak udělány pomocí kopírovaných booleanů.

![floors.png](./Screenshots/floors.png)

<p align="middle">
    <img src="./Screenshots/floors-extruded.png" height="300" />
</p>

### View

View je hlavní game object pokrývající nějaký pohled do světa, obsahuje jak samotného hráče a jeho UI tak i práci s ovládáním, dialogy atd..

<p align="middle">
    <img src="./Screenshots/View.png" width="30%" />
    <img src="./Screenshots/PlayerCapsule.png" width="30%" />
</p>

### GameState

GameState zase drží informace vázající se k aktuální scéně - v našem případě máme jednu herní Unity scénu. Stará se o dění v aktuální fázi hry a zvukový doprovod.

<p align="middle">
    <img src="./Screenshots/GameState.png" width="30%" />
    <img src="./Screenshots/Timer.png" width="30%" />
    <img src="./Screenshots/BackgroundAudioController.png" width="30%" />
</p>



### DialogueManager

DialogueManager je objekt přes který se spouští dialogy a vybírají odpovědi.

### IInteractable

Interaktivní objekty implementují nějaký komponent s interfacem IInteractable, práce s nimi je pak sjednocená na jednom místě. Interaktivní může být přímo nějaký objekt nebo jak je tomu např. u dialogů, nějaká oblast.

<p align="middle">
    <img src="./Screenshots/DialogHolderInteractive-trigger.png" width="60%" />
    <img src="./Screenshots/DialogHolderInteractive.png" width="30%" />
</p>

## Screenshoty

![1.PNG](./Screenshots/1.PNG)
![3.PNG](./Screenshots/3.PNG)
![6.PNG](./Screenshots/6.PNG)
![7.PNG](./Screenshots/7.PNG)
![8.PNG](./Screenshots/8.PNG)
![9.PNG](./Screenshots/9.PNG)
![10.PNG](./Screenshots/10.PNG)
![11.PNG](./Screenshots/11.PNG)
![12.PNG](./Screenshots/12.PNG)
![2.PNG](./Screenshots/2.PNG)
![4.PNG](./Screenshots/4.PNG)
![menu.png](./Screenshots/menu.png)


## List objektů

| Typ | Název | Popis |
| - | - | - |
| Prostředí | 9th-floor | https://help.fit.cvut.cz/rooms/map.html |
| Prostředí | 10th-floor | https://help.fit.cvut.cz/rooms/map.html |
| Prostředí | 11th-floor | https://help.fit.cvut.cz/rooms/map.html |
| Prostředí | 12th-floor | https://help.fit.cvut.cz/rooms/map.html |
| Prostředí | 13th-floor | https://help.fit.cvut.cz/rooms/map.html |
| Prostředí | 14th-floor | https://help.fit.cvut.cz/rooms/map.html |
| Objekt | elevator-floor-10-1 |  |
| Objekt | elevator-floor-10-2 |  |
| Objekt | elevator-floor-10-3 |  |
| Objekt | elevator-floor-10-4 |  |
| Objekt | elevator-floor-11-1 |  |
| Objekt | elevator-floor-11-2 |  |
| Objekt | elevator-floor-11-3 |  |
| Objekt | elevator-floor-14-1fbx |  |
| Objekt | elevator-floor-14-2 |  |
| Objekt | elevator-floor-14-3 |  |
| Objekt | elevator-floor-14-4 |  |
| Objekt | elevator-sign-A-bloody |  |
| Objekt | elevator-sign-A |  |
| Objekt | elevator-sign-B-bloody |  |
| Objekt | elevator-sign-B |  |
| Objekt | elevator-sign-C-bloody |  |
| Objekt | elevator-sign-C |  |
| Objekt | elevator-sign-D-bloody |  |
| Objekt | elevator-sign-D |  |
| Objekt | wall-sign-a-14 |  |
| Objekt | chair |  |
| Objekt | elevator-door |  |
| Objekt | elevator-number-pad |  |
| Světlo | fake recessed light | Osvětlení chodeb, area light |
| Objekt | file-drawer-bloody |  |
| Objekt | file-drawer |  |
| Objekt | flowerpot |  |
| Objekt | glass-door-1 |  |
| Objekt | glass-door-2 |  |
| Objekt | onesided-door-blood-1 |  |
| Objekt | onesided-door-clean |  |
| Prostředí | stairs |  |
| Objekt | double-door |  |
| Objekt | double-door-frame |  |
| Objekt | double-door-open |  |
| Prostředí | floor_hole-fill-1 |  |
| Prostředí | floor_hole-fill-2 |  |
| Prostředí | floor_top-fill-1 |  |
| Prostředí | floor_top-fill-2 |  |
| Objekt | kitchen-kabinet |  |
| Objekt | maxwell | Zdroj: https://sketchfab.com/3d-models/dingus-the-cat-2ca7f3c1957847d6a145fc35de9046b0 |
| Objekt | monitor-1fbx |  |
| Objekt | monitor-base |  |
| Objekt | mouse |  |
| Objekt | shelf |  |
| Objekt | single-door |  |
| Objekt | single-door-frame |  |
| Objekt | single-door-open-1 |  |
| Objekt | single-door-open-wide |  |
| Objekt | single-door-wide |  |
| Objekt | textbook-page |  |
| Objekt | thesis |  |
| Objekt | thesis-jorge |  |
| Objekt | trashcan-blue |  |
| Objekt | trashcan-red |  |
| Objekt | trashcan-yellow |  |
| Objekt | whiteboard |  |
| Objekt | window-single |  |
| Objekt | poof-1-blue |  |
| Objekt | poof-1-red |  |
| Objekt | poof-1-yellow |  |
| Objekt | poof-2-blue |  |
| Objekt | poof-quarter-circle-black |  |
| Objekt | poof-quarter-circle-blue |  |
| Objekt | poof-quarter-circle-red |  |
| Objekt | poof-quarter-circle-yellow |  |


## Textury:

| Typ | Název | Popis |
| - | - | - |
| Decal | bp |  |
| Decal | floppa |  |
| Decal | checker-map_tho |  |
| Decal | exit_sign |  |
| Decal | maxwell-diffuse | Zdroj: https://sketchfab.com/3d-models/dingus-the-cat-2ca7f3c1957847d6a145fc35de9046b0 |
| Decal | maxwell-normal | Zdroj: https://sketchfab.com/3d-models/dingus-the-cat-2ca7f3c1957847d6a145fc35de9046b0 |
| Decal | Maxwell_Roughness@channels=G | Zdroj: https://sketchfab.com/3d-models/dingus-the-cat-2ca7f3c1957847d6a145fc35de9046b0 |
| Decal | Maxwell_Whiskers | Zdroj: https://sketchfab.com/3d-models/dingus-the-cat-2ca7f3c1957847d6a145fc35de9046b0 |
| Decal | SVD_rozklad |  |
| Decal | ashes-1 |  |
| Decal | beige_wall |  |
| Decal | automat |  |
| Decal | Bez názvu-1 |  |
| Decal | blood-smudge-1 |  |
| Decal | blood-smudge-2 |  |
| Decal | blood-smudge-3 |  |
| Decal | blood-smudge-4 |  |
| Decal | bloody_hand |  |
| Decal | door-blood-1 |  |
| Decal | dukaz-je-trivialni |  |
| Decal | grades |  |
| Decal | konec |  |
| Decal | krvava-veta |  |
| Decal | neni_cesty_ven |  |
| Decal | odpad_sem |  |
| Decal | pozor_na_jazyk |  |
| Decal | save_me_mr_karacuba |  |
| Decal | spongebob |  |
| Decal | count_1 |  |
| Decal | count_2 |  |
| Decal | count_3 |  |
| Decal | count_4 |  |
| Decal | count_5 |  |
| Decal | building-a-ceiling |  |
| Decal | dejvice-sky |  |
| Decal | A |  |
| Decal | A-bloody |  |
| Decal | A-bloody-2 |  |
| Decal | B |  |
| Decal | B-bloody |  |
| Decal | C-bloody |  |
| Decal | D |  |
| Decal | D-bloody |  |
| Decal | exit_sign_1 |  |
| Decal | exit_sign_2 |  |
| Decal | exit_sign_3 |  |
| Decal | exit_sign_4 |  |
| Decal | 10 |  |
| Decal | 10-bloody-1 |  |
| Decal | 10-bloody-2 |  |
| Decal | 10-bloody-3 |  |
| Decal | 11 |  |
| Decal | 11-bloody-1 |  |
| Decal | 11-bloody-2 |  |
| Decal | 14 |  |
| Decal | 14-bloody-1 |  |
| Decal | 14-bloody-2 |  |
| Decal | 14-bloody-3 |  |
| Decal | wall-sign-a-10 |  |
| Decal | wall-sign-a-10-bloody |  |
| Decal | wall-sign-a-11 |  |
| Decal | wall-sign-a-11-bloody |  |
| Decal | wall-sign-a-14 |  |
| Decal | wall-sign-a-14-bloody |  |
| Decal | building-a-linoleum |  |
| Decal | bakalarka |  |
| Decal | bakalarka_jorge |  |
| Decal | dante |  |
| Decal | gato1 |  |
| Decal | gato2 |  |
| Decal | gato3 |  |
| Decal | gato4 |  |
| Decal | poster_1 |  |
| Decal | poster_fire |  |
| Decal | poster1 |  |
| Decal | poster-2 |  |
| Decal | poster-3 |  |
| Decal | building-a-linoleum |  |

## Postavy:

| Název | Zdroj |
| - | - |
| rp_carla_rigged |  Render People |
| rp_eric_rigged | Render People |
| rp_claudia_rigged | Render People |
| supervizor |https://sketchfab.com/3d-models/man-in-coat-character-human-riged-model-758a855697be47a1be0d707623e3907e |
| male_student_2 |https://sketchfab.com/3d-models/man-d91475da67924946b3b454daa17639a3 |
| male_student_1 | https://sketchfab.com/3d-models/young-character-human-riged-model-0953bc5fcd8c4af88e9bb0cf03b37b15 |
| female_student_1 | https://sketchfab.com/3d-models/female-civilian-v1-53e877e1b0f0446bbbf82809b185a215 |
| male_student_3| https://sketchfab.com/3d-models/rigged-t-pose-human-male-w-50-face-blendshapes-cc7e4596bcd145208a6992c757854c07 |
| female_student_2 | mixamo.com |
| female_student_3 | mixamo.com |
| male_student_4 | mixamo.com |
| male_student_5 | mixamo.com |
| male_student_6 | mixamo.com |
| male_student_7 | mixamo.com |

## Font:

| Název | Zdroj |
| - | - |
| Bohemian Typewriter | http://www.fontys.cz/fonts/12808/bohemian-typewriter.html |
