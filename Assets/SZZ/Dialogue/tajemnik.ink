VAR firstEncounter = true
VAR phase = 0 // 0 = pred zkouskou, 2 = behem zkousky, 3 = po zkousce (uspech)
VAR win = false

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}

=== first_encounter ===
Dobrý den, vítejte u zkoušky.
-> explain

=== repeated_encounter ===
{phase:
 - 0: 
      Tady nemáte co dělat. Vraťte se, až budete na řadě.
      -> ending
 - 1: 
      -> explain
 - 2:
      -> during_exam
 - 3:
      -> win_dialogue
}
=== explain ===

Postupně si Vás kolegové vyzkouší z oborové a společné otázky. Je jedno, kterou začnete. 

Připravil jste se dobře? Pokud ano, tak budou kolegové možná schovívavější...

Až zkoušení skončí, přijďte za mnou, abyste podepsal potřebné papíry, tedy... pokud to uděláte...
* [Rozumím, děkuji.]
-> goodbye
* [Zopakujete mi to, prosím?]
-> explain

=== during_exam ===

Pokračujte, nemáme na vás celý den.
-> goodbye

=== win_dialogue ===

Gratuluji, pane bakaláři. Nevím sice, jak jste to udělal... ale nějak se Vám podařilo přežít. 
Tak asi budete ta pověstná jehla v kupce sena. Ale nebojte, jistě se nevidíme naposledy, však uvidíme, jak se Vám povede příště...
~ win = true
-> ending

=== goodbye === 
Hodně štěstí.
-> ending

=== ending ===
-> END
