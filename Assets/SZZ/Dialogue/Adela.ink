VAR firstEncounter = true

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}


=== repeated_encounter ===

    + [Vysvětlila byste mi to prosím ještě jednou?]

-> explanation


=== first_encounter ===

Posaďte se, tady jste v bezpečí.
    + [Prosím, vysvětlila byste mi, jak to bude dneska probíhat?]
    
-> explanation


=== explanation ===

Samozřejmě. Jakmile tady skončíme, budete mít ještě chvíli času se rozdýchat, ale zamiřte co nejdřív na potítko. Rozhodně se moc necmrndejte kolem, aby nedošlo ke... komplikacím.

Potítko je o patro výš, tam se nahlásíte u dozoru a zpracujete otázky. Nemáte doufám žádné taháky? Kdyby Vás s nimi někdo chytil, byl byste... odstraněn.

Jakmile Vám na potítku dojde čas, musíte se přesunout do 14. patra, půjdete úplně na konec té dlouhé chodby do zasedačky. Tam nadejde zkouška,která se přirozeně bude odvíjet od toho, jak dobře si povedete s přípravou…

To je snad vše, tak hodně štěstí a moc se nebojte… bolí to jen trochu.
    + [Děkuji, rozumím. Jdu se připravit, na shledanou.]
        -> goodbye
    + [Vysvětlila byste mi to prosím ještě jednou?]
        -> explanation


=== goodbye ===

Kdybyste ještě potřeboval něco vysvětlit, nebojte se zastavit, jsem tady pro Vás.

[Měl bych se rychle podívat, jestli tady někdo nezapomněl něco, co by se mi mohlo hodit. Hmm, za co asi chytli všechny ty nebožáky kolem...]

-> END
