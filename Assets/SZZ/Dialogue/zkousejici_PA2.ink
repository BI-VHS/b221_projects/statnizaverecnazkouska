VAR firstEncounter = true
VAR available_mistakes = 0

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}

=== first_encounter ===
Tak se na Vás podíváme. Hmm... programování a algoritmizace... Ta vaše příprava teda trochu pokulhává, ale třeba se ještě předvedete. Takže.
-> first_question


=== repeated_encounter===
Moc bych být Vámi neprovokoval.
-> ending

=== first_question ===
Co je to dynamická vazba?
	* [Noo, ehm, eeee...]
		Aha, vidím, že jste se ale vůbec nepřipravil. To mě vůbec nepřekvapuje.
		~ decrement()
		-> first_question_decision
	* [Tak to znamená, že pro dynamicky alokované proměnné kopírujeme pouze ukazatel...]
		Proboha, radši už nepokračujte! Tohle opravdu nechci slyšet!
		~ decrement()
		-> first_question_decision
	* [To bude, že se volaná metoda určuje až v době výpočtu, určitě, to jsem se učil!]
		No. Tak dobrá, tohle by Vám šlo. Ale jsme teprve na začátku...
		-> first_question_decision

=== first_question_decision ===

{available_mistakes >= 0:
	Tak přejděme na další otázku.
	-> second_question
    - else:
	-> fail
}
		
=== second_question ===
Tak pane kolego, jak funguje zásobník?
	* To vím, to je typ LIPO, jakože dávám jenom na vrch a odtud zase beru, že ano.
		Já Vám dám takové LIPO... Vypadám snad jako cukrárna? Za tohle bych Vás měl vyhodit z okna!
		~ decrement()
		-> second_question_decision
	* Jó, to je ten jak se vždycky přistupuje jenom na začátek, ale přidává se na konec!
		Pane kolego, Vy vážně zkoušíte mou trpělivost, a můžu říct, že není veliká...
		~ decrement()
		-> second_question_decision
	* Uf, tak to si teďkom nejsem jistý, to bude asi to LIFO nebo tak nějak to bylo...
		Dobře, tohle Vám teda uznám, i když si vůbec nejsem jist, že to vážně umíte...
		-> second_question_decision

=== second_question_decision ===

{available_mistakes >= 0:
	Tak, poslední otázka.
	-> third_question
    - else:
	-> fail
}
	
=== third_question ===
Tak co s Vámi...Copak to jsou statické metody?
	* No to jsou ty takové, jak jsou podobné funkcím, ale nejsou v jednom konkrétním objektu...
		Vám to taky trvá půl roku, než se vymáčknete... ale dobrá.
		-> third_question_decision
	* Jasně, tak se říká třídním funkcím!
		Já už na Vás nemám nervy. Očividně jste se na to vykašlal, to se mi snad zdá!
		~ decrement()
		-> third_question_decision
	* Metody čeho?
		Metody čeho? Asi toho, jak Vás odsud co nejrychleji vyrazit!
		~ decrement()
		-> third_question_decision

=== third_question_decision ===

{available_mistakes >= 0:
	-> success
    - else:
	-> fail
}

=== fail ===
To snad není možné, co sem pouštějí za lidi. Ti studenti jsou rok od roku línější a línější, už ani na státnice se nepřipraví. 
Já bych to řešil razantně páni kolegové, takovou svoloč odsud nemůžeme pustit s titulem!
-> ending

=== success ===
{
 - available_mistakes == 0: 
      Vůbec jste mě, pane kolego, nepotěšil. Příprava mizerná, odpovědi váhavé, no... nemám chuť Vás nechat jít jen tak, ale kolegové už na mě tlačí, ať všechny nevyhazuju, tak budiž...
	-> ending
 - available_mistakes > 0 && available_mistakes < 3: 
      Nic moc, pane kolego, nic moc. Příště, pokud nějaké bude, se lépe připravte. Ale dobrá... ode mě se dostanete živ.
	-> ending
 - available_mistakes >= 3:
      Celkem solidní výkon, to musím uznat, ač se mi moc nezdá, že jste se na to skutečně připravil. Ale... tak běžte.
	-> ending
}


=== ending ===
-> END

=== function decrement() ===
~ available_mistakes--