VAR firstEncounter = true
VAR available_mistakes = 0

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}

=== first_encounter ===
Takže, pane kolego, já Vás vyzkouším z oborové otázky. Ah, pán je grafik, co? Takže si dáme nějakou lineární algebru!
-> first_question


=== repeated_encounter===
Co chcete?
-> ending

=== first_question ===
Co je to defekt zobrazení?
	* [Řekl bych, že dimenze jádra zobrazení.]
		Výborně, tak aspoň něco umíte.
		-> first_question_decision
	* [Defekt? To je nějaká vada.]
		Nějakou vadu máte spíš Vy. Špatně, špatně, špatně.
		~ decrement()
		-> first_question_decision
	* [To bude určitě hodnost matice zobrazení!]
		Fuj pane kolego, ale takové řeči na mě nezkoušejte, na tohle už jsem moc stará...
		~ decrement()
		-> first_question_decision

=== first_question_decision ===

{available_mistakes >= 0:
	Tak přejděme na další otázku.
	-> second_question
    - else:
	-> fail
}
		
=== second_question ===
Co znamená, že je zobrazení z X do Y surjektivní?
	* [To znamená, že pro všechna x,y z X platí, že pokud f(x)=f(y), potom x=y.]
		Ach jo, s Vámi je to těžké, to asi daleko nedotáhnete...
		~ decrement()
		-> second_question_decision
	* [Surjektivní? To je z francouzštiny.]
		Tak to určitě. Tady nejsme na jaderce, abychom se učili jazyky! Tohle mě dokáže akorát tak naštvat.
		~ decrement()
		-> second_question_decision
	* [To znamená, že pro všechna y z Y existuje x z X takové, že f(x)=y.]
		Takhle bych si to taky představovala.
		-> second_question_decision

=== second_question_decision ===

{available_mistakes >= 0:
	-> third_question
    - else:
	-> fail
}
	
=== third_question ===
Co znamená hodnota 0 mezi vlastními čísly matice?
	* [Matice je regulární.]
		Ale pane kolego. Takovou základní věc nevíte? To mě teda moc mrzí...
		~ decrement()
		-> third_question_decision
	* [No, to víte, to znamená, že řešení neexistuje!]
		S takovými odpověďmi už za chvilku nebudete existovat vy.
		~ decrement()
		-> third_question_decision
	* [Matice je singulární.]
		Vidíte, přece jen v té hlavě něco máte, i když toho není moc.
		-> third_question_decision

=== third_question_decision ===

{available_mistakes >= 0:
	-> fourth_question
    - else:
	-> fail
}

=== fourth_question ===
Kdy je soustava rovnic Ax = b řešitelná?
	* [Nikdy...]
		Nikdy... Vy asi nikdy nedostanete titul.
		~ decrement()
		-> fourth_question_decision
	* [Pokud h\(A\) = h\(A\|b\).]
		Tak to bychom měli.
		-> fourth_question_decision
	* [Pokud h\(A\) + 1 = h\(A\|b\).]
		Vy ani projít nechcete, že?
		~ decrement()
		-> fourth_question_decision

=== fourth_question_decision ===

{available_mistakes >= 0:
	-> success
    - else:
	-> fail
}

=== fail ===
Aha, pan kolega se nepřipravil, co s Vámi, pane kolego, copak uděláme? Vás už odsud nepustíme...
-> ending

=== success ===
{
 - available_mistakes == 0: 
      Tak nebylo to dobré. Nebylo to vůbec dobré, ale slituji se nad Vámi, naposledy.
	-> ending
 - available_mistakes > 0 && available_mistakes < 3: 
      Že bych byla spokojená, to ne, ale... zmizte mi z očí, než si to rozmyslím.
	-> ending
 - available_mistakes >= 3:
      Tentokrát jste to zvládl, ale uvidíme... ještě není všem dnům konec...
	-> ending
}


=== ending ===
-> END

=== function decrement() ===
~ available_mistakes--