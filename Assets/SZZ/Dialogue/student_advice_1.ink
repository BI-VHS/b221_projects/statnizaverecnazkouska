VAR result = 0 // 0 = neutral, 1 = + point, 2 = game over
VAR firstEncounter = true

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}


=== first_encounter ===
* [Pst, hej ... pomoz mi, prosím!]
Co chceš?
-> choice
===  choice ===
* [Kámo,prosím tě řekni mi, jak se počítá tuto nějaká matice, furt se tu na to ptají a já jsem úplně v prdeli…]
	Nevím, fakt sorry...
	-> ending
* [Nevíš, co je to šablona? Pozvu tě pak na pivo!]
	Slyšel jsem pivo? Jasně že ti poradím, to je parametrizovaná deklarace a definice funkce...
	~ result = 1
	* * [No jasně, už si vzpomínám! Děkuju!]
	-> ending
* [Buď prosím tě kolegiální a poraď mi!]
	Dej mi pokoj! Dozor! Dozor! Týpek vedle mě podvádí!
	~ result = 2
	-> ending

=== repeated_encounter ===
Dej pokoj.
-> ending

=== ending ===
-> END