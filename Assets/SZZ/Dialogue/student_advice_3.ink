VAR result = 0 // 0 = neutral, 1 = + point, 2 = game over
VAR firstEncounter = true

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}


=== first_encounter ===
* [Hej borče, neměl bys nějaké vědomosti nazbyt?]
No to teda nevím, nelíbí se mi tvůj tón...
-> choice
===  choice ===
* [Kámo prosím, já se fakt za chvilku bodnu...]
	Zas nebuď tak dramatický, já taky nechci vyletět!
	* * [Tak jsme na stejné lodi, vidíš!]
		Vidím, že už se s tebou nechci bavit, tak mi dej pokoj.
		A sakra, už jde k nám! Zničils mi život!
		~ result = 2
		-> ending
	* * [\(rozbrečíš se\)]
		Ale no tak dobře, koukám že tu máš lingebru, tak tady máš nějaké vzorečky a jdi už do prdele...
		~ result = 1
		-> ending
	* * [Prosím, já bez tebe vyletím...]
		No tak si leť, dej mi už pokoj!
		-> ending
* [Ale kolego, snad se nebudeš zlobit, já jen potřebuju pomoct!]
	Přestaň mě už prudit! Dozor, kolega tady vedle se mě snaží zkorumpovat!
	~ result = 2
	-> ending

=== repeated_encounter ===
Nech mě, nebo tě bodnu.
-> ending

=== ending ===
-> END