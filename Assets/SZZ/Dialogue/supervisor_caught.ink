VAR firstTime = true
VAR passed = true

{firstTime:
    -> first_encounter
  - else:
    -> repeated_encounter
}


=== first_encounter === // TODO

Pane kolego! Můžete mi vysvětlit, co to tady děláte?
    + [something bad]
        Tak hele debílku, myslíš si snad, že jsem úplně dementní? Moc dobře vidím, co tady děláš! Tohle není ani na disciplinární komisi... budeš toho litovat!
            -> fail
    + [something]
    
something

-> END


=== repeated_encounter === // TODO

something

    + [something]
    
something

-> fail


=== fail ===

~ passed = false

-> END
