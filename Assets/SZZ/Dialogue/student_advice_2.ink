VAR result = 0 // 0 = neutral, 1 = + point, 2 = game over
VAR firstEncounter = true

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}


=== first_encounter ===
* [Kamaráde, zachraň mě, já vůbec nic nevím!]
Ale já se bojím, že mě chytnou...
-> choice
===  choice ===
* [Prosím, já bych pro tebe udělal to samé...]
	A jo, nejsi ty ten týpek, od kterého jsem v prváku opsal Progtest? Tak jo, tady máš moje tajné poznámky, užívej je dobře.
	* * [Ty jsou super! Děkuju!]
	~ result = 1
	-> ending
	* * [Ty mi asi moc nepomůžou, nechceš mi to radši říct?]
	Ne, a víš ty co? Já si je zas vezmu, když si jich nevážíš.
	-> ending
* [Ale notak,on stejně nedává pozor!]
	Prosím, nech mě být, já nechci- 
	Do prdele! On si nás všimnul! Jsme v háji...
	~ result = 2
	-> ending

=== repeated_encounter ===
Nech mě, teď nemám čas.
-> ending

=== ending ===
-> END