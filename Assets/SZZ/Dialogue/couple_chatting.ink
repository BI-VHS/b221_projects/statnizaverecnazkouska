(Dívka) Slyšels to?
(Chlapec) Co myslíš?
(Dívka) Říkala to Adéla... že prý se dneska ještě nikdo nevrátil...
(Chlapec) To je blbost. Přece by všechny nevyhodili!
(Dívka) Já si myslím, že to klidně může být pravda... přece by nás neděsila jen tak.
(Chlapec) Tak v tom případě tam nejdu. Prostě ne.
(Dívka) A jak se odsud asi dostaneš? Víš, že všechny východy hlídají...
(Chlapec) Hej, pojďme se bavit trochu potišeji... myslím, že nás někdo poslouchá.

-> END