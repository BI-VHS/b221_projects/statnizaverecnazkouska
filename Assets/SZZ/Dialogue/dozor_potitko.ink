VAR firstEncounter = true
VAR fail = false

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}


=== repeated_encounter ===

Nemáte nic lepšího na práci?

-> ending


=== first_encounter ===

Vítejte, tady máte otázky, přímo na tělo. Doufám, že nemáte žádné taháky.
    * [Jasně, že mám.]
	Cos to řekl, debílku? Ukaž!
	* * [Tady máte.]
		Tak a dost… tohle je znevažování státní závěrečné zkoušky! Pryč s tebou!
		~ fail = true
		-> ending
	* * [To jsem si jen dělal srandu, haha…]
		Tohle ti přijde vtipné, blbečku? Za tohle bych tě měl okamžitě vyhodit! Ještě, že mám dobrou náladu… ale už si nic nezkoušej!
		-> goodbye
    * [Nemám, přísahám.]
	No, jen aby… kontrolovat se mi to nechce, tak radši jděte...
	-> goodbye
    


=== goodbye ===

Tak běžte dělat otázky. Čas běží.

[Sakra, mám v hlavě úplně prázdno... třeba by mi někdo tady mohl trochu poradit...]

-> ending

=== ending ===

-> END
