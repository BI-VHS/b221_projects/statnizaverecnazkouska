VAR firstEncounter = true

{firstEncounter:
    -> first_encounter
  - else:
    -> repeated_encounter
}


=== first_encounter ===

Hej...Neotálej tady, nebo tě dozor chytne. Hlavně nedělej nic podezřelého. Prosím, už nechci znovu vidět, jak-

To je jedno. Byl už jsi u Adély? Ne? Tak dělej, je tady hned za mnou, ať nepřijdeš pozdě. To by byl průšvih, to by byl průšvih. // (vyděšeně k sobě tiskne pobrečenou bakalářku)
    + [Co se ti stalo?]

Tam nahoře… to nic, to je jedno, pusť mě už- // (odchází do výtahu a ze scény)
-> ending

=== repeated_encounter ===

Nemluv na mě...
-> ending

=== ending ===

-> END
