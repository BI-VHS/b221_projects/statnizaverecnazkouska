using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using System;

[RequireComponent(typeof(DialogueHolder))]
public class PayRespectsInteraction : Interactable {
    [SerializeField] private int dropChance = 20; // 1 - 100
    [SerializeField] private GameObject pagePrefab;

    private System.Random random = new System.Random();
    private Animator animator;
    private DialogueHolder dialogueHolder;

    private void Awake() {
        animator = GetComponentInChildren<Animator>();
        dialogueHolder = GetComponent<DialogueHolder>();
    }

    private void Start() {
        if (GameState.Instance.currentLevel == 0) {
            GameState.Instance.onLevelChange += EnableAfterLevel0;
            this.enabled = false;
            return;
        }
    }

    public override void Interact() {
        dialogueHolder.Play();
    }

    public void DialogueStart(Story story) {
        bool foundCheat = GameState.Instance.currentLevel == 1 && random.Next(0, 100) <= dropChance;

        if (foundCheat)
            Instantiate(pagePrefab, transform.position + Vector3.up * .3f + Vector3.forward * .1f, Quaternion.identity);

        story.variablesState["foundCheat"] = foundCheat;
        enabled = false;
        //animator.enabled = false; TODO die here, this looks bad
    }

    private void EnableAfterLevel0() {
        if (GameState.Instance.currentLevel == 0)
            return;

        this.enabled = true;
        GameState.Instance.onLevelChange -= EnableAfterLevel0;
    }
}
