using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.SceneManagement;

public class SuccessfullStudent : MonoBehaviour {
    private bool firstEncounter = true;

    void Start() {
        GameState.Instance.onLevelChange += DestroyOnLevel1;
    }

    public void DialogueStart(Story story) {
        if (GameState.Instance.currentLevel == 0)
            story.variablesState["firstEncounter"] = firstEncounter;
        else
            story.variablesState["firstEncounter"] = false;
    }

    public void DialogueFinish(Story story) {
        firstEncounter = false;
    }

    private void DestroyOnLevel1() {
        if (GameState.Instance.currentLevel == 0)
            return;

        Destroy(gameObject);
        GameState.Instance.onLevelChange -= DestroyOnLevel1;
    }
}
