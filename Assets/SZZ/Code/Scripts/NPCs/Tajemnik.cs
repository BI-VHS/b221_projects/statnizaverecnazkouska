using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.SceneManagement;

public class Tajemnik : MonoBehaviour {
    private bool firstEncounter = true;

    public void DialogueStart(Story story) {
        var game = GameState.Instance;

        if (game.currentLevel == 4) // 4 jakoze mezilevel
            story.variablesState["firstEncounter"] = firstEncounter;
        else
            story.variablesState["firstEncounter"] = false;
        story.variablesState["win"] = game.level5Controller.win;
        story.variablesState["phase"] = game.level5Controller.phase;
        Debug.Log("Tajemnik - dialog start");
    }

    public void DialogueFinish(Story story) {
        var game = GameState.Instance;

        if (game.currentLevel == 4) {
            firstEncounter = false;
            game.ChangeLevel(5);
            game.level5Controller.phase = 2;
        }
        if (game.level5Controller.win)
            SceneManager.LoadScene(3);
    }
}
