using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Ink.Runtime;

public class NPCAdela : MonoBehaviour {
    private bool firstEncounter = true;

    public void DialogueStart(Story story) {
        story.variablesState["firstEncounter"] = firstEncounter;
    }

    public void DialogueFinish(Story story) {
        firstEncounter = false;

        if (GameState.Instance.currentLevel == 0)
            GameState.Instance.ChangeLevel(1);
    }
}
