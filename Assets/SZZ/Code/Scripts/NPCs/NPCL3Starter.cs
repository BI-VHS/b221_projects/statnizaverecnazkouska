using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;

public class NPCL3Starter : MonoBehaviour {
    private bool firstEncounter = true;

    public void DialogueStart(Story story) {
        if (GameState.Instance.currentLevel == 2)
            story.variablesState["firstEncounter"] = firstEncounter;
        else
            story.variablesState["firstEncounter"] = false;
    }

    public void DialogueFinish(Story story) {
        var game = GameState.Instance;

        if ((bool)story.variablesState["fail"])
            game.GameOver();

        if (game.currentLevel == 2) {
            firstEncounter = false;
            game.ChangeLevel(3);
        }
    }
}
