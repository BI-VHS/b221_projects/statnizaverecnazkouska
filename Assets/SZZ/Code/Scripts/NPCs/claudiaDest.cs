using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class claudiaDest : MonoBehaviour {
    public int pivotPoint = 0;

    void OnCollisionEnter(Collision other) {
        int[,] positions = new int[,] { { 38, 0, -3 }, { -38, 0, -3 }, { -38, 0, 5 }, { 17, 0, 3 }, { 17, 0, -10 } };
        if (other.gameObject.tag == "NPC") {
            // this.gameObject.transform.position = new Vector3(positions[pivotPoint, 0], positions[pivotPoint, 1], positions[pivotPoint, 2]);
            if (pivotPoint >= positions.Length / 3) {
                pivotPoint = 0;
            }
            this.gameObject.transform.localPosition = new Vector3(positions[pivotPoint, 0], positions[pivotPoint, 1], positions[pivotPoint, 2]);
            pivotPoint++;
        }
    }
}
