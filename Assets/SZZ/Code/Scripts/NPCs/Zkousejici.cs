using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Ink.Runtime;
using UnityEngine.SceneManagement;

public class Zkousejici : MonoBehaviour {
    private bool firstEncounter = true;

    public void DialogueStart(Story story) {
        var game = GameState.Instance;
        View.Instance.freezeMovement = true;

        if (game.currentLevel == 5)
            story.variablesState["firstEncounter"] = firstEncounter;
        else
            story.variablesState["firstEncounter"] = false;
        story.variablesState["available_mistakes"] = game.mistakesRemaining;
    }

    public void DialogueFinish(Story story) {
        var game = GameState.Instance;
        View.Instance.freezeMovement = false;

        if ((bool)story.variablesState["firstEncounter"]) // !! because class atribute firstEncounter is set at true at beginning -> talking to examiners would lead to false win
            game.level5Controller.questionsAnswered++;

        firstEncounter = false;
        game.mistakesRemaining = (int)story.variablesState["available_mistakes"];

        if (game.mistakesRemaining < 0)
            game.GameOver();
    }
}
