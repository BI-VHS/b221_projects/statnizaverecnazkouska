using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Ink.Runtime;
using UnityEngine.SceneManagement;

public class NPCEPAdvice : MonoBehaviour {
    private bool firstEncounter = true;
    private int result = 0;

    public void DialogueStart(Story story) {
        if (GameState.Instance.currentLevel == 3)
            story.variablesState["firstEncounter"] = firstEncounter;
        else
            story.variablesState["firstEncounter"] = false;
        story.variablesState["result"] = result;
    }

    public void DialogueFinish(Story story) {
        var game = GameState.Instance;

        if (game.currentLevel == 3)
            firstEncounter = false;
        switch (story.variablesState["result"]) {
            case 1: {
                    game.level3Controller.GetAdvice();
                    Debug.Log("+ advice point");
                    break;
                }
            case 2: {
                    game.GameOver();
                    break;
                }
            default:
                break;
        }
    }
}
