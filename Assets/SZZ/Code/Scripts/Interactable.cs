using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable {
    void Interact();

    string GetPrompt() {
        return "Interakce";
    }
}

public abstract class Interactable : MonoBehaviour, IInteractable {
    [SerializeField] protected string prompt = "Interakce";

    public abstract void Interact();

    public string GetPrompt() {
        return prompt;
    }
}
