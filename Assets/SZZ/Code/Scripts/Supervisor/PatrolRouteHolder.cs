using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrolRouteHolder : MonoBehaviour {
    [SerializeField] private GameObject routeObject;
    public List<Transform> route { get; private set; } = new List<Transform>();

    private void Awake() {
        foreach (Transform child in routeObject.GetComponentsInChildren<Transform>()) {
            if (child.gameObject != routeObject)
                route.Add(child);
        }
    }
}
