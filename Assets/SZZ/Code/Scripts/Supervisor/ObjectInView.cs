using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectInView : MonoBehaviour {
    [SerializeField] public GameObject target; // pls dont actually set :)
    [SerializeField, Range(0, 360)] private float fieldOfView = 100f;
    [SerializeField] private LayerMask obstacleMask;
    [SerializeField, Range(0, 2)] private float eyeLevel = 1.46f;
    [SerializeField, Min(0)] private float yCutoff = 4f;

    public Vector3 lastSeen { get; private set; } = Vector3.zero;
    public Vector3 lastSeenOrigin { get; private set; } = Vector3.zero;
    public float lastSeenTime { get; private set; } = 0;
    public event Action<ObjectInView> OnVisible;

    public new Collider collider { get; private set; }
    public Collider targetCollider { get; private set; }

    private void Awake() {
        collider = GetComponentInChildren<Collider>();
        targetCollider = target.GetComponentInChildren<Collider>();
    }

    private void FixedUpdate() {
        var selfPosition = EyePosition(collider);
        var targetPosition = EyePosition(targetCollider);


        if (Math.Abs(selfPosition.y - targetPosition.y) > yCutoff)
            return;

        var directionToTarget = (targetPosition - selfPosition).normalized;
        if (Vector3.Angle(collider.transform.forward, directionToTarget) <= fieldOfView / 2) {
            var distance = Vector3.Distance(selfPosition, targetPosition);
            if (!Physics.Raycast(selfPosition, directionToTarget, distance, obstacleMask)) {
                lastSeen = targetPosition;
                lastSeenOrigin = target.transform.position;
                lastSeenTime = Time.time;

                OnVisible?.Invoke(this);

                //Debug.DrawLine(selfPosition, targetPosition, Color.magenta, 3f, false);
            }
        }
    }

    private Vector3 EyePosition(Collider collider) {
        return collider.bounds.center + new Vector3(0, -collider.bounds.extents.y + eyeLevel, 0);
    }
}
