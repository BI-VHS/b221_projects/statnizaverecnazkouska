using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class SupervisorController : MonoBehaviour {
    private ObjectInView objectInView;
    private Animator animator;
    private NavMeshAgent navMeshAgent;
    private AudioSource audioSource;

    private bool footstepsPlayingBeforePause;

    private void Awake() {
        objectInView = GetComponent<ObjectInView>();
        animator = GetComponentInChildren<Animator>();
        navMeshAgent = GetComponent<NavMeshAgent>();
        audioSource = GetComponent<AudioSource>();
    }

    private void Start() {
        objectInView.OnVisible += OnPlayerVisible;
        View.Instance.OnPause += PauseFootsteps;
        View.Instance.OnResume += ResumeFootsteps;
    }

    private void OnDisable() {
        objectInView.OnVisible -= OnPlayerVisible;
        if (View.Instance is not null) {
            View.Instance.OnPause -= PauseFootsteps;
            View.Instance.OnResume -= ResumeFootsteps;
        }
    }

    private void FixedUpdate() {
        animator.SetFloat("horizontalVelocity", Vector3.Scale(navMeshAgent.velocity, Vector3.one - Vector3.up).magnitude);
    }

    private void OnPlayerVisible(ObjectInView sender) {
        if (GameState.Instance.currentLevel == 1)
            animator.SetBool("chasing", true);
    }

    private void PauseFootsteps() {
        footstepsPlayingBeforePause = audioSource.isPlaying;
        if (audioSource.isPlaying)
            audioSource.Pause();
    }

    private void ResumeFootsteps() {
        if (footstepsPlayingBeforePause)
            audioSource.Play();
    }
}
