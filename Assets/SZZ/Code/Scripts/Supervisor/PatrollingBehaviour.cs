using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PatrollingBehaviour : StateMachineBehaviour {
    // TODO stringtohash optimization
    [SerializeField, Range(0, 1)] private float waitChance = .25f;

    private NavMeshAgent navMeshAgent;
    private PatrolRouteHolder patrolRoute;

    private int patrolPointIndex = 0;
    private bool waiting = false;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //Debug.Log("supervisor patrolling");

        if (navMeshAgent is null) {
            navMeshAgent = animator.GetComponentInParent<NavMeshAgent>();
            patrolRoute = animator.GetComponentInParent<PatrolRouteHolder>();
        }

        navMeshAgent.SetDestination(patrolRoute.route[patrolPointIndex].position);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (NavMeshUtils.AgentArrivedOrGaveUp(navMeshAgent) && !waiting && !animator.GetBool("chasing")) {
            patrolPointIndex++;
            if (patrolPointIndex >= patrolRoute.route.Count)
                patrolPointIndex = 0;

            if (Random.Range(0f, 1f) <= waitChance) {
                animator.SetTrigger("wait");
                waiting = true;
            }
            else {
                //Debug.Log("supervisor patrolling next point");
                navMeshAgent.SetDestination(patrolRoute.route[patrolPointIndex].position);
            }
        }
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        navMeshAgent.isStopped = true;
        navMeshAgent.ResetPath();
        waiting = false;
    }
}
