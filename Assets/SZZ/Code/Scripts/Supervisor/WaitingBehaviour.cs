using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitingBehaviour : StateMachineBehaviour {
    // TODO look around

    [SerializeField, Min(0)] private float minTime = 1f;
    [SerializeField, Min(0)] private float maxTime = 8f;

    private PatrolRouteHolder patrolRoute;
    private AudioSource audioSource;

    private Coroutine coroutine = null;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (patrolRoute is null) {
            patrolRoute = animator.GetComponentInParent<PatrolRouteHolder>();
            audioSource = animator.GetComponentInParent<AudioSource>();
        }

        audioSource.Pause();
        coroutine = patrolRoute.StartCoroutine(Wait(Random.Range(minTime, maxTime), animator));
    }

    override public void OnStateExit(Animator animator, AnimatorStateInfo animatorStateInfo, int layerIndex) {
        audioSource.Play();

        if (coroutine is not null) {
            patrolRoute.StopCoroutine(coroutine);
            coroutine = null;
        }
    }

    private IEnumerator Wait(float delayTime, Animator animator) {
        //Debug.Log($"supervisor waiting for {delayTime}s");

        yield return new WaitForSeconds(delayTime);

        //Debug.Log($"supervisor waiting done");

        animator.SetTrigger("stopWaiting");
        coroutine = null;
    }
}
