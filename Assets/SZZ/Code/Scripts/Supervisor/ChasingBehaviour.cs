using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class ChasingBehaviour : StateMachineBehaviour {
    [SerializeField] private float speedMultiplier = 4f;
    [SerializeField] private float catchDistance = .7f;

    private NavMeshAgent navMeshAgent;
    private ObjectInView playerInView;
    private AudioSource audioSource;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //Debug.Log($"supervisor chasing");

        if (navMeshAgent is null) {
            navMeshAgent = animator.GetComponentInParent<NavMeshAgent>();
            playerInView = animator.GetComponentInParent<ObjectInView>();
            audioSource = animator.GetComponentInParent<AudioSource>();
        }

        navMeshAgent.speed *= speedMultiplier;
        audioSource.pitch *= speedMultiplier;

        playerInView.OnVisible += UpdateDestination;
        UpdateDestination(playerInView);
    }

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (NavMeshUtils.AgentArrivedOrGaveUp(navMeshAgent))
            animator.SetBool("chasing", false);

        if (Vector3.Distance(playerInView.targetCollider.bounds.center, playerInView.collider.bounds.center) <= catchDistance) {
            //Debug.Log("CAUGHT!");
            // TODO supervisor_caught.ink dialog
            GameState.Instance.GameOver();
        }
    }

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        playerInView.OnVisible -= UpdateDestination;
        navMeshAgent.isStopped = true;
        navMeshAgent.ResetPath();

        navMeshAgent.speed /= speedMultiplier;
        audioSource.pitch = 1;
    }

    private void UpdateDestination(ObjectInView sender) {
        navMeshAgent.SetDestination(sender.lastSeenOrigin);
    }
}
