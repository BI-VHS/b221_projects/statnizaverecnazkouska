using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PageDropper : MonoBehaviour {
    [SerializeField, Min(0)] private float minDelay = 20f;
    [SerializeField, Min(0)] private float maxDelay = 180f;
    [SerializeField, Min(0)] private int count = 2;
    [SerializeField] private GameObject pagePrefab;

    private int dropped = 0;

    private new Collider collider;

    private void Awake() {
        collider = GetComponentInChildren<Collider>();
    }

    private void Start() {
        if (dropped < count)
            StartCoroutine(DropAfter(Random.Range(minDelay, maxDelay)));
    }

    private IEnumerator DropAfter(float delayTime) {
        yield return new WaitForSeconds(delayTime);

        if (GameState.Instance.currentLevel == 1) {
            Instantiate(pagePrefab, collider.bounds.center - transform.forward * collider.bounds.extents.z * 1.1f, Quaternion.identity);
            dropped++;
        }

        if (dropped < count && GameState.Instance.currentLevel < 2)
            StartCoroutine(DropAfter(Random.Range(minDelay, maxDelay)));
    }
}
