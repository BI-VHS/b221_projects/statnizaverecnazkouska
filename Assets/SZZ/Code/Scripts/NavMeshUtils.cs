using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

static class NavMeshUtils {
    public static bool AgentArrivedOrGaveUp(NavMeshAgent agent) {
        if (!agent.pathPending && agent.remainingDistance <= agent.stoppingDistance && (!agent.hasPath || agent.velocity.sqrMagnitude == 0f))
            return true;

        return false;
    }
}
