using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundAudioController : MonoSingleton<BackgroundAudioController> {
    [SerializeField] public AudioSource OSTSource;
    [SerializeField] public bool playOST = true;

    public AudioSource currentAudio;
    private Coroutine waitForAudioCoroutine;
    private Action onAudioEnd;

    private void Start() {
        PlayOST();
    }

    public void Play(AudioSource audio, Action onAudioEnd = null) {
        //Stop(playOST = false);

        currentAudio = audio;
        this.onAudioEnd = onAudioEnd;

        currentAudio.Play();
        waitForAudioCoroutine = StartCoroutine(WaitForAudioEnd());
    }

    public void Stop(bool runOnAudioEnd = false, bool startOST = true) {
        if (currentAudio is null || !currentAudio.isPlaying)
            return;

        StopCoroutine(waitForAudioCoroutine);
        currentAudio.Stop();

        if (runOnAudioEnd)
            onAudioEnd?.Invoke(); // TODO fix looping

        if (currentAudio.isPlaying)
            return;

        if (playOST && startOST)
            PlayOST();
        else {
            waitForAudioCoroutine = null;
            onAudioEnd = null;
        }
    }

    private IEnumerator WaitForAudioEnd() {
        yield return new WaitWhile(() => currentAudio.isPlaying);

        waitForAudioCoroutine = null;
        Stop(true);
    }

    public void PlayOST(bool loop = true) {
        OSTSource.loop = true;
        Play(OSTSource);
    }
}

