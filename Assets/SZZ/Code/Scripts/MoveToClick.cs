using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.InputSystem;

public class MoveToClick : MonoBehaviour {
    [SerializeField] private new Camera camera;

    private NavMeshAgent navMeshAgent;
    private InputAction leftMouseClick;

    private void Awake() {
        leftMouseClick = new InputAction(binding: "<Mouse>/leftButton");
        leftMouseClick.performed += ctx => LeftMouseClicked();
        leftMouseClick.Enable();

        navMeshAgent = GetComponent<NavMeshAgent>();
    }

    private void LeftMouseClicked() {
        Ray ray = camera.ScreenPointToRay(Mouse.current.position.ReadValue());
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit)) {
            navMeshAgent.SetDestination(hit.point);
        }
    }
}
