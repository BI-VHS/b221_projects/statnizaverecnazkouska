using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class VictoryScreen : MonoBehaviour
{
    private void Start(){
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }
    
    public void ReturnToMenu ()
    {
        SceneManager.LoadScene(0);
    }
}
