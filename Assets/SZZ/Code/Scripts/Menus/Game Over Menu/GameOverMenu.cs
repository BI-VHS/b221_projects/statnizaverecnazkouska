using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{

    void Start ()
    {
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;
    }

    public void RetryGame ()
    {
        SceneManager.LoadScene(1); // File -> Build settings, game has scene with index 1
    }

    public void QuitToMenu ()
    {
        SceneManager.LoadScene(0); // File -> Build settings, main menu has scene with index 0
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
