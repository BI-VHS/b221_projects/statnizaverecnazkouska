using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class PauseMenu : MonoBehaviour {
    [SerializeField] private GameObject menuUI;

    private GameObject eventSystemPreviousSelected = null;

    private InputAction pauseAction;

    private void Start() {

        menuUI.SetActive(View.Instance.isPaused);

        pauseAction = View.Instance.playerInput.actions["UI/Cancel"];
        pauseAction.started += TogglePause;
    }

    private void OnDisable() {
        pauseAction.started -= TogglePause;
    }

    private void TogglePause(InputAction.CallbackContext context) {
        if (View.Instance.isPaused)
            ResumeGame();
        else
            PauseGame();
    }

    public void ResumeGame() {
        View.Instance.Resume();

        menuUI.SetActive(false);
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;

        View.Instance.playerInput.SwitchCurrentActionMap("Player");
        StartCoroutine(DelayUIActionMapEnable());

        EventSystem.current.SetSelectedGameObject(eventSystemPreviousSelected);
    }

    public void PauseGame() {
        eventSystemPreviousSelected = EventSystem.current.currentSelectedGameObject;

        View.Instance.Pause();

        menuUI.SetActive(true);
        Cursor.visible = true;
        Cursor.lockState = CursorLockMode.None;

        View.Instance.playerInput.SwitchCurrentActionMap("UI");
    }

    public void ReturnToMenu() {
        menuUI.SetActive(false);
        View.Instance.ReturnToMenu();
    }

    public void QuitGame() {
        View.Instance.Quit();
    }

    private IEnumerator DelayUIActionMapEnable() {
        yield return null;

        View.Instance.inputActionAsset.FindActionMap("UI").Enable();
    }
}
