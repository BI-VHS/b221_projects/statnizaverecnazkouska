using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class PlayerActionsController : MonoBehaviour {

    [Header("Interaction")]
    [SerializeField] private float interactionDistance = 1.8f;
    [SerializeField] private LayerMask interactionLayerMask;
    public GameObject interactionTrigger { get; private set; } = null;

    private FirstPersonController firstPersonController;
    private NoClipController noClipController;

    private InputAction interactAction;
    private InputAction payRespectsAction;
    private InputAction noClipAction;

    private void Awake() {
        firstPersonController = GetComponent<FirstPersonController>();
        noClipController = GetComponent<NoClipController>();

        firstPersonController.enabled = true;
        noClipController.enabled = false;
    }

    private void Start() {
        var playerInput = View.Instance.playerInput;

        interactAction = playerInput.actions["Player/Interact"];
        payRespectsAction = playerInput.actions["Player/PayRespects"];
        noClipAction = playerInput.actions["Player/NoClip"];

        interactAction.started += Interact;
        payRespectsAction.started += PayRespects;
        noClipAction.started += NoClip;
    }

    private void OnDisable() {
        interactAction.started -= Interact;
        payRespectsAction.started -= PayRespects;
        noClipAction.started -= NoClip;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.CompareTag("Interactable"))
            interactionTrigger = other.gameObject;
    }

    private void OnTriggerExit(Collider other) {
        if (other.gameObject == other.gameObject)
            interactionTrigger = null;
    }

    public GameObject ShootInteractionRay() {
        // TODO more reach on ground, less horizontally and upwards
        RaycastHit hitData;
        Ray ray = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));

        Debug.DrawRay(ray.origin, ray.direction * interactionDistance, Color.magenta, 3);

        if (Physics.Raycast(ray, out hitData, interactionDistance, interactionLayerMask))
            return hitData.transform.gameObject;

        return null;
    }

    public GameObject GetLookedAtInteractable(string tag = "Interactable") {
        var hitObject = ShootInteractionRay();

        if (hitObject is not null && hitObject.CompareTag(tag)) {
            Behaviour interactable = (Behaviour)hitObject.GetComponent<IInteractable>();
            if (interactable is not null && interactable.enabled)
                return hitObject;
        }

        return null;
    }

    private void Interact(InputAction.CallbackContext context) {
        var hitObject = GetLookedAtInteractable();

        if (hitObject is not null)
            hitObject.GetComponent<IInteractable>().Interact();
        else
            interactionTrigger?.GetComponent<IInteractable>()?.Interact();
    }

    private void PayRespects(InputAction.CallbackContext context) {
        var hitObject = GetLookedAtInteractable("PayRespects");

        if (hitObject is not null)
            hitObject.GetComponent<IInteractable>().Interact();
    }

    private void NoClip(InputAction.CallbackContext context) {
        if (firstPersonController.enabled) {
            firstPersonController.enabled = false;
            noClipController.enabled = true;
        }
        else {
            firstPersonController.enabled = true;
            noClipController.enabled = false;
        }
    }
}
