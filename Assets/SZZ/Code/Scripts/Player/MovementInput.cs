using UnityEngine;
using UnityEngine.InputSystem;

public class MovementInput : MonoSingleton<MovementInput> {
    [Header("Character Input Values")]
    public Vector2 move;
    public Vector2 look;
    public bool jump;
    public bool sprint;

    [Header("Movement Settings")]
    public bool analogMovement;

    [Header("Mouse Cursor Settings")]
    public bool cursorLocked = true;
    public bool cursorInputForLook = true;

    private PlayerInput playerInput;

    private InputAction moveAction;
    private InputAction lookAction;
    private InputAction jumpAction;
    private InputAction sprintAction;

    protected override void Awake() {
        base.Awake();

        playerInput = GetComponent<PlayerInput>();
    }

    private void Start() {
        moveAction = playerInput.actions["Player/Move"];
        lookAction = playerInput.actions["Player/Look"];
        jumpAction = playerInput.actions["Player/Jump"];
        sprintAction = playerInput.actions["Player/Sprint"];
    }

    private void Update() {
        if (!View.Instance.freezeMovement) {
            Move();
            Look();
            Jump();
            Sprint();
        }
    }

    private void Move() {
        MoveInput(moveAction.ReadValue<Vector2>());
    }

    private void Look() {
        LookInput(lookAction.ReadValue<Vector2>());
    }

    private void Jump() {
        JumpInput(jumpAction.WasPressedThisFrame()); // && !dialogueManager.dialogueIsPlaying);
    }

    private void Sprint() {
        SprintInput(sprintAction.IsPressed());
    }

    public void MoveInput(Vector2 newMoveDirection) {
        move = newMoveDirection;
    }

    public void LookInput(Vector2 newLookDirection) {
        look = newLookDirection;
    }

    public void JumpInput(bool newJumpState) {
        jump = newJumpState;
    }

    public void SprintInput(bool newSprintState) {
        sprint = newSprintState;
    }

    private void OnApplicationFocus(bool hasFocus) {
        SetCursorState(cursorLocked);
    }

    private void SetCursorState(bool newState) {
        Cursor.lockState = newState ? CursorLockMode.Locked : CursorLockMode.None;
    }
}
