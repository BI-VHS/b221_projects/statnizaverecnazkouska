using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(DialogueHolder))]
public class Player : MonoBehaviour {
    [SerializeField] private GameObject _cameraRoot;
    public GameObject cameraRoot { get { return _cameraRoot; } }

    private DialogueHolder dialogueHolder;

    private void Awake() {
        dialogueHolder = GetComponent<DialogueHolder>();
    }

    public void PlaySelfDialogue(TextAsset inkJSON) {
        dialogueHolder.inkJSON = inkJSON;
        dialogueHolder.Play();
    }
}
