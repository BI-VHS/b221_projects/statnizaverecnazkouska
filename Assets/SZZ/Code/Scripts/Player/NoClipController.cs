using UnityEngine;
using UnityEngine.InputSystem;

[RequireComponent(typeof(CharacterController))]
public class NoClipController : MonoBehaviour {

    [Header("Player")]
    [Tooltip("Move speed of the character in m/s")]
    public float MoveSpeed = 4.0f;
    [Tooltip("Sprint speed of the character in m/s")]
    public float SprintSpeed = 6.0f;
    [Tooltip("Rotation speed of the character")]
    public float RotationSpeed = 1.0f;
    [Tooltip("Acceleration and deceleration")]
    public float SpeedChangeRate = 10.0f;

    [Header("Cinemachine")]
    [Tooltip("How far in degrees can you move the camera up")]
    public float TopClamp = 90.0f;
    [Tooltip("How far in degrees can you move the camera down")]
    public float BottomClamp = -90.0f;

    // cinemachine
    private float cinemachineTargetPitch;

    // player
    private float speed;
    private float rotationVelocity;

    private Player player;
    private CharacterController characterController;

    private const float ignoreThreshold = 0.01f;

    private bool IsCurrentDeviceMouse {
        get {
            return View.Instance.playerInput.currentControlScheme == "KeyboardMouse";
        }
    }

    private void Awake() {
        player = GetComponent<Player>();
        characterController = GetComponent<CharacterController>();
    }

    private void Start() {
        Physics.IgnoreLayerCollision(7, 0, true); // Default
        Physics.IgnoreLayerCollision(7, 9, true); // Ground
        player.cameraRoot.transform.localRotation = Quaternion.identity;
    }

    private void OnDisable() {
        Physics.IgnoreLayerCollision(7, 0, false); // Default
        Physics.IgnoreLayerCollision(7, 9, false); // Ground

        transform.localRotation = Quaternion.Euler(0, transform.localRotation.eulerAngles.y, 0);
    }

    private void Update() {
        Move();
    }

    private void LateUpdate() {
        CameraRotation();
    }

    private void CameraRotation() {
        var input = MovementInput.Instance;

        // if there is an input
        if (input.look.sqrMagnitude >= ignoreThreshold) {
            //Don't multiply mouse input by Time.deltaTime
            float deltaTimeMultiplier = IsCurrentDeviceMouse ? 1.0f : Time.deltaTime;

            cinemachineTargetPitch += input.look.y * RotationSpeed * deltaTimeMultiplier;
            rotationVelocity = input.look.x * RotationSpeed * deltaTimeMultiplier;

            // clamp our pitch rotation
            cinemachineTargetPitch = ClampAngle(cinemachineTargetPitch, BottomClamp, TopClamp);

            // rotate the player left and right
            transform.localRotation = Quaternion.Euler(0, transform.localRotation.eulerAngles.y, 0);
            transform.Rotate(Vector3.up * rotationVelocity);
            transform.Rotate(Vector3.right * cinemachineTargetPitch);
        }
    }

    private void Move() {
        var input = MovementInput.Instance;

        // set target speed based on move speed, sprint speed and if sprint is pressed
        float targetSpeed = input.sprint ? SprintSpeed : MoveSpeed;

        // a simplistic acceleration and deceleration designed to be easy to remove, replace, or iterate upon

        // note: Vector2's == operator uses approximation so is not floating point error prone, and is cheaper than magnitude
        // if there is no input, set the target speed to 0
        if (input.move == Vector2.zero) targetSpeed = 0.0f;

        // a reference to the players current horizontal velocity
        float currentSpeed = characterController.velocity.magnitude;

        float speedOffset = 0.1f;
        float inputMagnitude = input.analogMovement ? input.move.magnitude : 1f;

        // accelerate or decelerate to target speed
        if (currentSpeed < targetSpeed - speedOffset || currentSpeed > targetSpeed + speedOffset) {
            // creates curved result rather than a linear one giving a more organic speed change
            // note T in Lerp is clamped, so we don't need to clamp our speed
            speed = Mathf.Lerp(currentSpeed, targetSpeed * inputMagnitude, Time.deltaTime * SpeedChangeRate);

            // round speed to 3 decimal places
            speed = Mathf.Round(speed * 1000f) / 1000f;
        }
        else {
            speed = targetSpeed;
        }

        // normalise input direction
        Vector3 inputDirection = new Vector3(input.move.x, 0.0f, input.move.y).normalized;

        // note: Vector2's != operator uses approximation so is not floating point error prone, and is cheaper than magnitude
        // if there is a move input rotate player when the player is moving
        if (input.move != Vector2.zero) {
            // move
            inputDirection = transform.right * input.move.x + transform.forward * input.move.y;
        }

        // move the player
        characterController.Move(inputDirection.normalized * (speed * Time.deltaTime));
    }


    private static float ClampAngle(float lfAngle, float lfMin, float lfMax) {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
}
