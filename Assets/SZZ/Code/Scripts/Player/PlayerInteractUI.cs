using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerActionsController))]
public class PlayerInteractUI : MonoBehaviour {
    [SerializeField] private GameObject interactPrompt;
    [SerializeField] private GameObject payRespectsPrompt;

    private bool showInteractionPrompt = false;
    private bool showPayRespactsPrompt = false;

    private PlayerActionsController playerActionsController;

    private void Awake() {
        playerActionsController = GetComponent<PlayerActionsController>();
    }

    private void FixedUpdate() {
        showInteractionPrompt = false;
        showPayRespactsPrompt = false;

        // TODO prompt
        var lookedAtInteract = playerActionsController.GetLookedAtInteractable();
        showInteractionPrompt = lookedAtInteract is not null;

        var lookedAtPayRespects = playerActionsController.GetLookedAtInteractable("PayRespects");
        showPayRespactsPrompt = lookedAtPayRespects is not null;

        if (playerActionsController.interactionTrigger is not null)
            showInteractionPrompt = true;

    }

    private void Update() {
        var dialogueManager = DialogueManager.Instance;

        TogglePrompt(interactPrompt, showInteractionPrompt && !dialogueManager.dialogueIsPlaying);
        TogglePrompt(payRespectsPrompt, showPayRespactsPrompt && !dialogueManager.dialogueIsPlaying);
    }

    private void TogglePrompt(GameObject obj, bool active) {
        if (obj.activeSelf != active)
            obj.SetActive(active);
    }
}
