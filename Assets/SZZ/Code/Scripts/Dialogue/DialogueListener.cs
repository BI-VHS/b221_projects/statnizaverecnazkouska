using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueListener : MonoSingleton<DialogueListener> {
    [SerializeField] private float maxDistance = 5f;

    private void FixedUpdate() {
        if (DialogueManager.Instance.dialogueIsPlaying && !CanHear())
            DialogueManager.Instance.Break();
    }

    private bool CanHear() {
        if (DialogueManager.Instance.source is null)
            return false;

        return Vector3.Distance(transform.position, DialogueManager.Instance.source.transform.position) <= maxDistance;
    }
}
