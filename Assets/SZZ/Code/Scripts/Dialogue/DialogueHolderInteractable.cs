using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Ink.Runtime;

public class DialogueHolderInteractable : DialogueHolder, IInteractable {
    public string GetPrompt() {
        return "Promluvit si";
    }

    public void Interact() {
        Play();
    }
}
