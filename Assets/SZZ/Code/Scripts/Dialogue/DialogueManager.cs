using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Ink.Runtime;
using UnityEngine.InputSystem;
using UnityEngine.EventSystems;

public class DialogueManager : MonoSingleton<DialogueManager> {
    [Header("Dialogue UI")]
    [SerializeField] private GameObject dialoguePanel;
    [SerializeField] private TextMeshProUGUI dialogueText;

    [Header("Perception")]
    [SerializeField] private float charactersPerSecond = 200f * 4f / 60f;

    [Header("Choices UI")]
    [SerializeField] private GameObject choiceButtonsParent;
    [SerializeField] private GameObject choiceButtonPrefab;
    [SerializeField] private float xOffset = 300;

    private Story currentStory = null;
    public DialogueHolder source { get; private set; } = null;
    public bool dialogueIsPlaying { get; private set; } = false;
    private GameObject[] choiceButtons = null;

    private InputAction skipAction;

    private Coroutine storyCoroutine = null;
    private Coroutine exitDialogueCoroutine = null;

    private void Start() {
        dialoguePanel.SetActive(false);

        View.Instance.OnResume += EnableChoices;
        View.Instance.OnPause += DisableChoices;

        skipAction = View.Instance.playerInput.actions["UI/Submit"];
        skipAction.started += ContinueStory;
    }

    private void OnDisable() {
        View.Instance.OnResume -= EnableChoices;
        View.Instance.OnPause -= DisableChoices;

        skipAction.started -= ContinueStory;
    }

    public void EnterDialogueMode(TextAsset inkJSON, DialogueHolder source) {
        if (dialogueIsPlaying)
            return;

        if (exitDialogueCoroutine is not null) {
            StopCoroutine(exitDialogueCoroutine);
            exitDialogueCoroutine = null;
        }

        currentStory = new Story(inkJSON.text);
        this.source = source;
        dialogueIsPlaying = true;
        dialoguePanel.SetActive(true);

        source.OnStart(currentStory);

        ContinueStory();
    }

    private void ContinueStory(InputAction.CallbackContext context) {
        if (choiceButtons is null)
            ContinueStory();
    }

    public void ContinueStory() {
        if (!dialogueIsPlaying)
            return;

        if (storyCoroutine is not null) {
            StopCoroutine(storyCoroutine);
            storyCoroutine = null;
        }

        if (currentStory.canContinue) {
            dialogueText.text = currentStory.Continue();
            if (currentStory.currentChoices.Count != 0)
                DisplayChoices();
            else
                storyCoroutine = StartCoroutine(DelayContinue(dialogueText.text.Length / charactersPerSecond));
        }
        else {
            source.OnFinish(currentStory);
            ExitDialogueMode();
        }
    }

    public void Break() {
        if (!dialogueIsPlaying || exitDialogueCoroutine is not null)
            return;

        source.OnBreak(currentStory);
        ExitDialogueMode();
    }

    public void ExitDialogueMode() {
        if (!dialogueIsPlaying || exitDialogueCoroutine is not null)
            return;

        ClearChoices();
        source = null;
        currentStory = null;
        dialogueText.text = "";
        dialoguePanel.SetActive(false);
        exitDialogueCoroutine = StartCoroutine(DelayExitDialogue());
    }

    public void MakeChoice(int choiceIndex) {
        if (!dialogueIsPlaying)
            return;

        ClearChoices();
        currentStory.ChooseChoiceIndex(choiceIndex);
        source.OnChoice(currentStory);
        ContinueStory();
    }

    private void DisplayChoices() {
        List<Choice> currentChoices = currentStory.currentChoices;

        if (currentChoices.Count == 0)
            return;

        choiceButtons = new GameObject[currentChoices.Count];
        int i = 0;
        foreach (var choice in currentChoices) {
            var position = new Vector3(xOffset * (i - (currentChoices.Count - 1) / 2f), 0, 0);
            choiceButtons[i] = Instantiate(choiceButtonPrefab, Vector3.zero, Quaternion.identity, choiceButtonsParent.transform);
            choiceButtons[i].transform.localPosition = position;
            choiceButtons[i].GetComponentInChildren<TextMeshProUGUI>().text = choice.text;
            choiceButtons[i].GetComponent<DialogueChoiceButton>().index = i;
            i++;
        }

        EventSystem.current.SetSelectedGameObject(choiceButtons[0]);
    }

    private void ClearChoices() {
        if (choiceButtons is null)
            return;

        foreach (var choice in choiceButtons)
            Destroy(choice);

        choiceButtons = null;
    }

    private void DisableChoices() {
        if (choiceButtons is null)
            return;

        foreach (var choice in choiceButtons)
            choice.GetComponent<UnityEngine.UI.Button>().interactable = false;
    }

    private void EnableChoices() {
        if (choiceButtons is null)
            return;

        foreach (var choice in choiceButtons)
            choice.GetComponent<UnityEngine.UI.Button>().interactable = true;
    }

    private IEnumerator DelayContinue(float delayTime) {
        yield return new WaitForSeconds(delayTime);

        storyCoroutine = null;
        ContinueStory();
    }

    private IEnumerator DelayExitDialogue() {
        yield return null;

        dialogueIsPlaying = false;
        exitDialogueCoroutine = null;
    }
}
