using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueChoiceButton : MonoBehaviour {
    public int index;
    private bool interactive = false;

    private void Start() {
        interactive = true;
    }

    public void OnClick() {
        if (interactive)
            DialogueManager.Instance.MakeChoice(index);
    }
}
