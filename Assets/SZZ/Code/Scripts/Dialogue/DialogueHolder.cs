using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Ink.Runtime;

public class DialogueHolder : MonoBehaviour {
    [SerializeField] public TextAsset inkJSON;
    [SerializeField] public UnityEvent<Story> onStart;
    [SerializeField] public UnityEvent<Story> onChoice;
    [SerializeField] public UnityEvent<Story> onBreak;
    [SerializeField] public UnityEvent<Story> onFinish;

    public void Play() {
        DialogueManager.Instance.EnterDialogueMode(inkJSON, this);
    }

    public void OnStart(Story story) {
        onStart?.Invoke(story);
    }

    public void OnChoice(Story story) {
        onChoice?.Invoke(story);
    }

    public void OnBreak(Story story) {
        onBreak?.Invoke(story);
    }

    public void OnFinish(Story story) {
        onFinish?.Invoke(story);
    }
}
