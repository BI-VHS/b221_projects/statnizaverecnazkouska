using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FlickerLightsControl : MonoBehaviour
{

    public bool isFlickering = false;
    private float timeDelay;

    private bool flickering = false;
    public float flickerFrequency = 0.07f;

    private new GameObject light;
    private GameObject panel;

    public float minOn = 5.0f;
    public float maxOn = 9.0f;

    public float minFlickerFor = 1.0f;
    public float maxFlickerFor = 2.0f;

    

    public Material onLightEmissionMaterial;
    public Material offLightEmissionMaterial;

    void Start()
    {
        light = this.transform.GetChild(0).gameObject;
        panel = this.transform.GetChild(1).gameObject;
        StartCoroutine(FlickerControl());
    }


    void Update()
    {
        //Debug.Log("isFlickering: " + isFlickering);
        if (isFlickering == true)
        {
            if (flickering == false)
            {
                StartCoroutine(FlickeringLight());
            }
        }
        else
        {
            light.GetComponent<Light>().enabled = true;
            panel.GetComponent<Renderer>().material = onLightEmissionMaterial;
        }

        IEnumerator FlickeringLight()
        {
            flickering = true;
            light.GetComponent<Light>().enabled = false;
            panel.GetComponent<Renderer>().material = offLightEmissionMaterial;
            yield return new WaitForSeconds(flickerFrequency);

            light.GetComponent<Light>().enabled = true;
            panel.GetComponent<Renderer>().material = onLightEmissionMaterial;
            yield return new WaitForSeconds(flickerFrequency);
            flickering = false;
        }
    }


    IEnumerator FlickerControl()
    {
        while (true)
        {
            isFlickering = false;
            timeDelay = Random.Range(minOn, maxOn);
            yield return new WaitForSeconds(timeDelay);

            isFlickering = true;
            timeDelay = Random.Range(minFlickerFor, maxFlickerFor);
            yield return new WaitForSeconds(timeDelay); 
        }  
    }
}
