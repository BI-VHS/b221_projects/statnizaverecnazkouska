using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using Cinemachine;
using UnityEngine.SceneManagement;
using System;

public class View : MonoSingleton<View> {
    [SerializeField] private Player _player;
    public Player player { get { return _player; } }

    [SerializeField] private PlayerInput _playerInput;
    public PlayerInput playerInput { get { return _playerInput; } }

    [SerializeField] private InputActionAsset _inputActionAsset;
    public InputActionAsset inputActionAsset { get { return _inputActionAsset; } }

    [SerializeField] private CinemachineVirtualCamera _followCamera;
    public CinemachineVirtualCamera followCamera { get { return _followCamera; } }

    public bool isPaused { get; private set; } = false;
    public bool freezeMovement { get; set; } = false;

    public Action OnResume { get; set; }
    public Action OnPause { get; set; }

    public void Pause() {
        // Log("view.Pause()");
        isPaused = true;
        Time.timeScale = 0f;
        OnPause?.Invoke();
    }

    public void Resume() {
        // Log("view.Resume()");
        isPaused = false;
        Time.timeScale = 1f;
        OnResume?.Invoke();
    }

    public void ReturnToMenu() {
        Resume();
        SceneManager.LoadScene(0);
    }

    public void Quit() {
        // Log("view.Quit()");
        Application.Quit();
    }
}
