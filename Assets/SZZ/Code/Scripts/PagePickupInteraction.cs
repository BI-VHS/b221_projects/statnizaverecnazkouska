using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PagePickupInteraction : Interactable {
    private void Start() {
        GameState.Instance.onLevelChange += DestroyAfterLevel1;
    }

    public override void Interact() {
        var game = GameState.Instance;

        var origCheatCount = game.cheatsPickedUp;
        game.level1Controller.PickupCheat();

        if (origCheatCount != game.cheatsPickedUp)
            Destroy(gameObject);
    }

    private void DestroyAfterLevel1() {
        if (GameState.Instance.currentLevel <= 1)
            return;

        Destroy(gameObject);
        GameState.Instance.onLevelChange -= DestroyAfterLevel1;
    }
}
