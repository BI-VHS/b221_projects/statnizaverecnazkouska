using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level5Controller : MonoBehaviour {
    [SerializeField] int questionsNeeded = 2;

    private int _questionsAnswered = 0; // TODO show checkmarks
    public int questionsAnswered {
        get {
            return _questionsAnswered;
        }
        set {
            _questionsAnswered = value;
            if (_questionsAnswered >= questionsNeeded) {
                win = true;
                phase = 3;
            }
        }
    }
    public bool win = false;
    public int phase = 0;

    private void Start() {
        var game = GameState.Instance;
        game.currentQuest = "! Slož státní závěrečnou zkoušku.";
        game.mistakesRemaining = game.availableMistakes;
        // timer.Run(60, () => { game.StartLevel2(); });
    }
}
