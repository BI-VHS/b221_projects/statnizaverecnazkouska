using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using TMPro;

public class GameState : MonoSingleton<GameState> {
    [SerializeField] public int maxCheats = 5;
    [SerializeField] TMP_Text questField;

    public int currentLevel { get; private set; } = 0; // MUST be initialized at 0
    [NonSerialized] public int cheatsPickedUp = 0;
    [NonSerialized] public int adviceAmount = 0;
    public int availableMistakes {
        get {
            return (int)Mathf.Floor((cheatsPickedUp + adviceAmount) / 2);
        }
    }
    [NonSerialized] public int mistakesRemaining = 0;

    private string _currentQuest = "";
    public string currentQuest {
        get {
            return _currentQuest;
        }
        set {
            _currentQuest = value;
            questField.text = _currentQuest;
        }
    }

    [NonSerialized] public Level0Controller level0Controller;
    [NonSerialized] public Level1Controller level1Controller;
    [NonSerialized] public Level2Controller level2Controller;
    [NonSerialized] public Level3Controller level3Controller;
    [NonSerialized] public Level4Controller level4Controller;
    [NonSerialized] public Level5Controller level5Controller;

    public Action onLevelChange { get; set; }

    protected override void Awake() {
        base.Awake();

        level0Controller = GetComponent<Level0Controller>();
        level1Controller = GetComponent<Level1Controller>();
        level2Controller = GetComponent<Level2Controller>();
        level3Controller = GetComponent<Level3Controller>();
        level4Controller = GetComponent<Level4Controller>();
        level5Controller = GetComponent<Level5Controller>();

        onLevelChange += FlashQuestField;

        ChangeLevel(0);
    }

    private void OnDisable() {
        onLevelChange -= FlashQuestField;
    }

    public void GameOver() {
        SceneManager.LoadScene(2);
    }

    public void ChangeLevel(int newLevel) {
        if (currentLevel == newLevel)
            return;

        currentLevel = newLevel;

        level0Controller.enabled = currentLevel == 0;
        level1Controller.enabled = currentLevel == 1;
        level2Controller.enabled = currentLevel == 2;
        level3Controller.enabled = currentLevel == 3;
        level4Controller.enabled = currentLevel == 4;
        level5Controller.enabled = currentLevel == 5;

        onLevelChange?.Invoke();
    }

    private void FlashQuestField() {
        StartCoroutine(_FlashQuestField());
    }

    private IEnumerator _FlashQuestField() {
        for (int i = 0; i < 5; i++) {
            questField.alpha = 0;
            yield return new WaitForSeconds(.08f);
            questField.alpha = 1;
            yield return new WaitForSeconds(.12f);
        }
    }
}
