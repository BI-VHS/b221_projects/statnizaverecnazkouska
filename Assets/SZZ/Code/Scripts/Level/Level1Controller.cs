using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class Level1Controller : MonoBehaviour {
    [Header("UI")]
    [SerializeField] private string cheatCountFieldPrefix = "Zvednuté taháky: ";
    [SerializeField] private TMP_Text cheatCountField;
    [SerializeField] private Image cheatCountImageContainer;
    [SerializeField] private Sprite[] cheatCountImages;

    [Header("Timer")]
    [SerializeField] private Timer timer;
    [SerializeField] private float timerLength = 100;

    [Header("Self dialogue")]
    [SerializeField] private TextAsset maxCheatsInkJSON;


    private void Start() {
        var game = GameState.Instance;
        game.currentQuest = "! Najdi na patře zapomenuté taháky. Nenech. Se. Chytit.";

        timer.Run(timerLength, () => { game.ChangeLevel(2); });

        cheatCountField.enabled = true;
        cheatCountField.text = cheatCountFieldPrefix;
        cheatCountImageContainer.enabled = false;
    }

    private void OnDisable() {
        if (cheatCountField is not null) // scene was destroyed
            return;

        cheatCountField.text = "";
        cheatCountField.enabled = false;
        cheatCountImageContainer.enabled = false;
    }

    public void PickupCheat() {
        var game = GameState.Instance;

        // TODO pickup cheats only in level 1?
        if (game is null || game.currentLevel != 1)
            return;

        cheatCountImageContainer.enabled = true;

        if (game.cheatsPickedUp < game.maxCheats)
            game.cheatsPickedUp++;

        SetImage(game.cheatsPickedUp);

        if (game.cheatsPickedUp >= game.maxCheats)
            View.Instance.player.PlaySelfDialogue(maxCheatsInkJSON);
    }

    private void SetImage(int index) {
        if (cheatCountImages.Length >= index && index > 0)
            cheatCountImageContainer.sprite = cheatCountImages[index - 1];
    }
}
