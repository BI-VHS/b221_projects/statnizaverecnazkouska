using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.UI;

public class Timer : MonoBehaviour {
    [SerializeField] private string textPrefix = "Time left: ";
    [SerializeField] private TMP_Text timerTextField;
    [Header("Audio")]
    [SerializeField] private float fashHeartbeatThreshold = 30;
    [SerializeField] private AudioSource[] heartbeatSources;
    [Header("Heartbeat animation")]
    [SerializeField] private Image[] heartbeatAnimationSources;
    [SerializeField] private Image heartbeatBackground;

    public float remainingTime { get; private set; } = 0;
    public bool running { get; private set; } = false;
    private Action onFinish = null;

    void Start() {
        timerTextField.enabled = false;
        timerTextField.text = remainingTime.ToString("f0");
        heartbeatBackground.enabled = false;
        // promin
        for (int i = 0; i < 3; i++) {
            heartbeatAnimationSources[i].enabled = false;
        }
    }

    void Update() {
        if (!running)
            return;
        if (remainingTime - Time.deltaTime > 0) {
            remainingTime -= Time.deltaTime;
            PlayHeartbeat();
        }
        else {
            remainingTime = 0;
            running = false;
            heartbeatBackground.enabled = false;
            for (int i = 0; i < 3; i++) {
                heartbeatAnimationSources[i].enabled = false;
            }
            onFinish?.Invoke();
        }

        timerTextField.text = textPrefix + remainingTime.ToString("f0");
    }

    public void Run(float timeAmount, Action onFinish = null) {
        remainingTime = timeAmount;
        running = true;
        this.onFinish = onFinish;
        timerTextField.enabled = true;
        heartbeatBackground.enabled = true;
        PlayHeartbeat();
    }

    private void PlayHeartbeat() {
        if (!running)
            return;

        var source = heartbeatSources[0];
        heartbeatAnimationSources[0].enabled = true;
        var backgroundAudioController = BackgroundAudioController.Instance;

        if (remainingTime <= fashHeartbeatThreshold) {
            heartbeatAnimationSources[0].enabled = false;
            source = remainingTime > fashHeartbeatThreshold / 2 ? heartbeatSources[1] : heartbeatSources[2];
            // znovu promin
            if (remainingTime > fashHeartbeatThreshold / 2) {
                heartbeatAnimationSources[1].enabled = true;
            }
            else {
                heartbeatAnimationSources[1].enabled = false;
                heartbeatAnimationSources[2].enabled = true;
            }
        }

        if (!backgroundAudioController.currentAudio.isPlaying || backgroundAudioController.currentAudio == backgroundAudioController.OSTSource) {
            backgroundAudioController.Play(source, () => PlayHeartbeat());
        }
    }
}
