using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;

public class Level3Controller : MonoBehaviour {
    [SerializeField] private string adviceCountFieldPrefix = "Obdržené rady: ";
    [SerializeField] private TMP_Text adviceCountField;
    [SerializeField] private Image adviceCountImageContainer;
    [SerializeField] private Sprite[] adviceCountImages;
    [SerializeField] private Timer timer;

    private void Start() {
        var game = GameState.Instance;
        game.currentQuest = "! Požádej spolužáky, ať ti poradí s otázkou.";

        timer.Run(40, () => { game.ChangeLevel(4); });

        adviceCountField.enabled = true;
        adviceCountField.text = adviceCountFieldPrefix;
        adviceCountImageContainer.enabled = false;
    }

    private void OnDisable() {
        if (adviceCountField is not null) // scene was destroyed
            return;

        adviceCountField.text = "";
        adviceCountField.enabled = false;
        adviceCountImageContainer.enabled = false;
    }

    public void GetAdvice() {
        var game = GameState.Instance;

        if (game is null || game.currentLevel != 3)
            return;
        game.adviceAmount++;
        adviceCountImageContainer.enabled = true;
        SetImage(game.adviceAmount);
    }

    private void SetImage(int index) {
        if (adviceCountImages.Length >= index && index > 0)
            adviceCountImageContainer.sprite = adviceCountImages[index - 1];
    }
}
