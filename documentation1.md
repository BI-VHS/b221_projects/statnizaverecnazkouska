![10_adela_kancl](./Screenshots/10_adela_kancl.png)
10_adela_kancl

![10_lobby](./Screenshots/10_lobby.png)
10_lobby

![10_lobby_2](./Screenshots/10_lobby_2.png)
10_lobby_2

![10_lobby_3](./Screenshots/10_lobby_3.png)
10_lobby_3

![10_respirium_1](./Screenshots/10_respirium_1.png)
10_respirium_1

![10_respirium_2](./Screenshots/10_respirium_2.png)
10_respirium_2

![10_respirium_3](./Screenshots/10_respirium_3.png)
10_respirium_3

![10_stairs](./Screenshots/10_stairs.png)
10_stairs

![11_lobby](./Screenshots/11_lobby.png)
11_lobby


## List objektů

| Typ | Název | Popis |
| - | - | - |
| Prostředí | 9th-floor | https://help.fit.cvut.cz/rooms/map.html |
|  | 10th-floor | https://help.fit.cvut.cz/rooms/map.html |
|  | 11th-floor | https://help.fit.cvut.cz/rooms/map.html |
|  | 12th-floor | https://help.fit.cvut.cz/rooms/map.html |
|  | 13th-floor | https://help.fit.cvut.cz/rooms/map.html |
|  | 14th-floor | https://help.fit.cvut.cz/rooms/map.html |
| Decal | blood-1 |  |
|  | blood-2 |  |
|  | elevator-floor-10-1 |  |
|  | elevator-floor-10-2 |  |
|  | elevator-floor-10-3 |  |
|  | elevator-floor-10-4 |  |
|  | elevator-floor-11-1 |  |
|  | elevator-floor-11-2 |  |
|  | elevator-floor-11-3 |  |
|  | elevator-floor-14-1fbx |  |
|  | elevator-floor-14-2 |  |
|  | elevator-floor-14-3 |  |
|  | elevator-floor-14-4 |  |
|  | elevator-sign-A-bloody |  |
|  | elevator-sign-A |  |
|  | elevator-sign-B-bloody |  |
|  | elevator-sign-B |  |
|  | elevator-sign-C-bloody |  |
|  | elevator-sign-C |  |
|  | elevator-sign-D-bloody |  |
|  | elevator-sign-D |  |
|  | poster-1 |  |
|  | poster2 |  |
|  | poster3 |  |
|  | wall-sign-a-10-bloody |  |
|  | wall-sign-a-10 |  |
|  | wall-sign-a-11-bloody |  |
|  | wall-sign-a-11 |  |
|  | wall-sign-a-14-bloody |  |
|  | wall-sign-a-14 |  |
| Static | exit-sign-1 |  |
|  | exit-sign-2 |  |
|  | exit-sign-3 |  |
|  | chair |  |
|  | elevator-door |  |
|  | elevator-number-pad |  |
|  | fake recessed light | Osvětlení chodeb, area light |
|  | file-drawer-bloody |  |
|  | file-drawer |  |
|  | flowerpot |  |
|  | glass-door-1 |  |
|  | glass-door-2 |  |
|  | onesided-door-blood-1 |  |
|  | onesided-door-clean |  |
|  | stairs |  |
|  | table |  |
| Postava | rp_carla_rigged | Zdroj: Render People |
|  | rp_eric_rigged | Zdroj: Render People |
|  | rp_claudia_rigged | Zdroj: Render People |
