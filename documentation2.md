## Cyklické činnosti

Dozor se dynamicky přesouvá mezi svými stavy chování - Obcházení, pronásledování a rozhlížení se kolem místa kde naposledy spatřil hráče.

![supervisor-states](./Screenshots/supervisor-states.png)

![chasing](./Screenshots/chasing.gif)

## Produkce

Stránky ze skript tj. taháky potřebuje hráč pro usnadnění dokončení hry. Hlídkující dozor je občas upustí nebo je lze nalézt u omráčených studentů, končí v rukou hráče, který je vymění za volné špatné odpovědi.

![page-drop](./Screenshots/page-drop.gif)

![page-pickup](./Screenshots/page-pickup.gif)

## List objektů

| Typ | Název | Popis |
| - | - | - |
| Static | textbook-page |  |
|  | door-0.826 |  |
|  | door-1.4 |  |
|  | door-2.14 |  |
|  | door-2.92 |  |
| Postava | man-in-coat-human | Zdroj: Sketchfab - mohsenmousavi2313 |
| Skybox | Dejvice Sky | poskládaný z výhledů z budovy A |

## Nová podoba prostor

![adela-office](./Screenshots/adela-office.PNG)
Nová kancelář Adély

![conference-room](./Screenshots/conference-room.PNG)
Zasedací místnost TH:A-1455

![respirium-10](./Screenshots/respirium_10.PNG)
Respirium v 10. patře

![respirium-10-2](./Screenshots/respirium_10_2.PNG)
Respirium v 10. patře
